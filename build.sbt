name := "BaseDeDatos"

version := "1.0"

scalaVersion := "2.11.7"

libraryDependencies ++= {
  val sprayV = "1.3.3"
  Seq(
    "io.spray"                        %%  "spray-can"                   % sprayV,
    "io.spray"                        %%  "spray-routing"               % sprayV,
    "io.spray"                        %%  "spray-http"                  % sprayV,
    "mysql"                           %  "mysql-connector-java"         % "5.1.35",
    "com.typesafe.slick"              %%  "slick"                       % "3.0.2",
    "ch.qos.logback"                  % "logback-classic"               % "1.0.13",
    "com.typesafe.akka"               % "akka-actor_2.11"               % "2.3.4",
    "com.typesafe.akka"               % "akka-slf4j_2.11"               % "2.3.4",
    "org.json4s"                      %% "json4s-native"                % "3.2.11",
    "org.aspectj"                     % "aspectjweaver"                 % "1.7.2",
    "org.aspectj"                     % "aspectjrt"                     % "1.7.2"
  )
}

javaOptions += "-javaagent:" + System.getProperty("user.home") + "/.ivy2/cache/org.aspectj/aspectjweaver/jars/aspectjweaver-1.7.2.jar"

fork := true

resolvers ++= Seq(
  "Spray repository" at "http://repo.spray.io",
  "Typesafe repository" at "http://repo.typesafe.com/typesafe/releases/"
)

mainClass := Some("db.Main")