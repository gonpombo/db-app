package app.models

import scala.slick.driver.MySQLDriver.simple._

/**
 * Created by gpombo on 9/9/15.
 */
case class Person (id: Option[Long],
                  name: String)

class Persons(tag: Tag) extends Table[Person](tag, "person") {
  def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
  def name = column[String]("name")
  def * = (id.?, name) <> (Person.tupled, Person.unapply)
}