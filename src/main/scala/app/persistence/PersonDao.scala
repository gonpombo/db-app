package app.persistence

import app.database.MysqlConnector
import app.models._
import slick.driver.MySQLDriver.api._
import slick.lifted.TableQuery

import scala.concurrent.Future

/**
 * Created by gpombo on 9/9/15.
 */

object PersonDao extends TableQuery(new Persons(_)) with MysqlConnector {
  def findByName(name: String): Future[Seq[Person]] = {
    val a = this.filter(_.name === name).result
    db.run(a)
  }
}