package app.rest

import akka.actor.Actor
import app.models.Person
import app.persistence.PersonDao
import app.{Configs => c}
import org.json4s.{DefaultFormats, Formats}
import org.slf4j.LoggerFactory
import spray.http.StatusCodes.InternalServerError
import spray.httpx.Json4sSupport
import spray.routing.HttpService

import scala.concurrent.ExecutionContext.Implicits.global
import scala.util.{Failure, Success}

/**
 * Created by gpombo on 9/24/15.
 */
class ApiService extends Actor with HttpService with Json4sSupport {
  implicit def json4sFormats: Formats = DefaultFormats
  def actorRefFactory = context
  def receive = runRoute(
    path("person") {
      complete {
        LoggerFactory.getLogger(this.getClass).info("Get ~ Person")
        Person(Some(1), "asd")
      }
    } ~
    path ("person2") {
      onComplete (PersonDao.findByName("pepe")) {
        case Success(a) => complete(a)
        case Failure(e) => complete(InternalServerError, e.getMessage)
      }
    }
  )
}
