package app

import akka.actor.{Props, ActorSystem}
import akka.io.IO
import app.database.MysqlConnector
import app.rest.ApiService
import spray.can.Http

object Main extends App with MysqlConnector{

  implicit val system = ActorSystem("dbApp")
  val service = system.actorOf(Props[ApiService], "spray-service") //.withRouter to multiple actors.
  IO(Http) ! Http.Bind(service, interface = "localhost", port = 8080)
}