package app.database

import app.{Configs => c}

import scala.slick.driver.MySQLDriver.simple._
/**.
 * Created by gpombo on 9/11/15.
 */
trait MysqlConnector {
  implicit lazy val db = MysqlConnector.db
}

object MysqlConnector {
  lazy val db = {
    Database.forURL(url = c.dbUrl,
      user = c.dbUser, password = c.dbPass, driver = c.dbDriver, keepAliveConnection = true)
  }
}
