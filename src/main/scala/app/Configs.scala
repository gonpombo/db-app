package app

import com.typesafe.config.ConfigFactory
import org.slf4j.{Logger, LoggerFactory}

/**
 * Created by gpombo on 9/11/15.
 */
object Configs {
  val c = ConfigFactory.load()
  val dbUrl = c.getString("mysql.url")
  val dbUser = c.getString("mysql.user")
  val dbPass = c.getString("mysql.password")
  val dbDriver = c.getString("mysql.driver")

  val log: Logger = LoggerFactory.getLogger(this.getClass)
}
